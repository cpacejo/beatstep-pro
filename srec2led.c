#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define BASE 0x8000000ul
#define BUF_SIZE 4096u
#define CHUNK_SIZE 0x800u

static int read_srec(
    int* const type, uint_fast32_t* const address,
    unsigned char* const data, size_t* const data_size)
{
    if (scanf("S%1i", type) < 1) return -1;

    switch (*type)
    {
    case 0:
    case 1:
    case 5:
    case 9:
        if (scanf("%2zX%4" SCNxFAST32, data_size, address) < 2) return -1;
        if (*data_size < 3u) return -1;
        *data_size -= 3u;
        break;

    case 2:
    case 6:
    case 8:
        if (scanf("%2zX%6" SCNxFAST32, data_size, address) < 2) return -1;
        if (*data_size < 4u) return -1;
        *data_size -= 4u;
        break;

    case 3:
    case 7:
        scanf("%2zX%8" SCNxFAST32, data_size, address);
        if (*data_size < 5u) return -1;
        *data_size -= 5u;
        break;
    }

    if (*data_size > BUF_SIZE) return -1;

    for (size_t i = 0; i < *data_size; ++i)
    {
        unsigned temp;
        if (scanf("%2X", &temp) < 1) return -1;
        data[i] = temp;
    }

    if (scanf("%*2X ") < 0) return -1;

    return 0;
}

static void emit_led_chunk(
    const uint_fast32_t address,
    unsigned char* const data, size_t length,
    const bool final)
{
    bool all_set = true;
    for (size_t i = 0; i < length && all_set; ++i)
        all_set = all_set && data[i] == 0xFFu;

    if (all_set) length = 0u;

    const unsigned addr_csum = (address >> 16) + ((address >> 8) & 0xFFu) + (address & 0xFFu);
    const unsigned len_csum = (length >> 8) + (length & 0xFFu);

    unsigned csum = 0u;

    printf("%04zX6534EA3D", length + (length > 0u ? 0x1Cu : 0x10u) + (final ? 0x0Cu : 0u));
    csum += 0x65u + 0x34u + 0xEAu + 0x3Du;

    printf("09%06" PRIXFAST32 "0000", address);
    csum += 0x09u + addr_csum;

    if (!all_set)
    {
        printf("0A000000%04zX", length);
        csum += 0x0Au + len_csum;

        for (size_t i = 0; i < length; ++i)
        {
            printf("%02X", data[i]);
            csum += data[i];
        }

        printf("0B%06" PRIXFAST32 "%04zX", address, length);
        csum += 0x0Bu + addr_csum + len_csum;
    }

    if (final)
    {
        printf("0D%06" PRIXFAST32 "0000", address);
        csum += 0x0Du + addr_csum;

        printf("0E%06" PRIXFAST32 "0000", address);
        csum += 0x0Eu + addr_csum;
    }

    printf("00000000%02X%02X", (-csum) & 0xFFu, ((-csum) >> 8) & 0xFFu);
}

int main()
{
    int srec_type;
    uint_fast32_t srec_address;
    unsigned char srec_data[BUF_SIZE];
    size_t srec_data_size;

    uint_fast32_t address = 0xFFFFFFFFu;
    unsigned offset = 0u;

    unsigned file_csum = 0u;

    while (read_srec(&srec_type, &srec_address, &srec_data[offset], &srec_data_size) >= 0)
    {
        switch (srec_type)
        {
        case 1:
        case 2:
        case 3:
            assert(srec_data_size <= CHUNK_SIZE);
            assert(srec_address >= BASE);

            if (srec_address != address + offset || CHUNK_SIZE - offset < srec_data_size)
            {
                if (offset > 0u)
                {
                    for (unsigned i = 0; i < offset; ++i)
                        file_csum += srec_data[i];
                    emit_led_chunk(address - BASE, srec_data, offset, false);
                }

                memcpy(srec_data, &srec_data[offset], srec_data_size);

                address = srec_address;
                offset = 0u;
            }

            offset += srec_data_size;
            break;
        }
    }

    assert(offset > 0u);
    assert(offset >= 2u);
    for (unsigned i = 0; i < offset - 2u; ++i)
        file_csum += srec_data[i];
    srec_data[offset - 1u] = (-file_csum) >> 8;
    srec_data[offset - 2u] = -file_csum;

    emit_led_chunk(address - BASE, srec_data, offset, true);

    return 0;
}
