#include <errno.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define BASE 0x8000000ul
#define BUF_SIZE 4096u
#define CHUNK_SIZE 0x800u

static int emit_srec(
    const int type, const uint_fast32_t address,
    const unsigned char* const data, const size_t data_size)
{
    if (data_size > 128u)
    {
        errno = EINVAL;
        return -1;
    }

    unsigned checksum = 0u;

    printf("S%i", type);

    switch (type)
    {
    case 0:
    case 1:
    case 5:
    case 9:
        printf("%02zX%04" PRIxFAST32, data_size + 3u, address);
        checksum += 3u;
        break;

    case 2:
    case 6:
    case 8:
        printf("%02zX%06" PRIxFAST32, data_size + 4u, address);
        checksum += 4u;
        break;

    case 3:
    case 7:
        printf("%02zX%08" PRIxFAST32, data_size + 5u, address);
        checksum += 5u;
        break;
    }

    checksum += data_size;
    checksum += address >> 24;
    checksum += (address >> 16) & 0xFFu;
    checksum += (address >> 8) & 0xFFu;
    checksum += address & 0xFFu;

    for (size_t i = 0; i < data_size; ++i)
    {
        printf("%02X", data[i]);
        checksum += data[i];
    }

    printf("%02X\n", ~checksum & 0xFFu);

    return 0;
}

static int read_led(
    int* const type, uint_fast32_t* const address,
    unsigned char* const data, size_t* const data_size)
{
    static size_t chunk_size = 0u;

    if (chunk_size == 0u)
    {
        uint_fast32_t magic;
        if (scanf("%4zX%8" SCNxFAST32, &chunk_size, &magic) < 1) return -1;
        if (chunk_size < 4u || magic != 0x6534ea3dul)
        {
            errno = EIO;
            return -1;
        }

        chunk_size -= 4u;
    }

    if (chunk_size < 6u)
    {
        errno = EIO;
        return -1;
    }

    unsigned utype;
    if (scanf("%2X%6" SCNxFAST32 "%4zX", &utype, address, data_size) < 3) return -1;
    *type = utype;

    chunk_size -= 6u;

    if (*type != 10)
        return 0;

    if (chunk_size < *data_size)
    {
        errno = EIO;
        return -1;
    }

    chunk_size -= *data_size;

    for (size_t i = 0u; i < *data_size; ++i)
    {
        unsigned x;
        if (scanf("%2X", &x) < 1) return -1;
        data[i] = x;
    }

    return 0;
}

int main()
{
    emit_srec(0, 0, (const unsigned char*) "HDR", 3);

    int type;
    uint_fast32_t address = 0u, address_temp;
    unsigned char data[BUF_SIZE];
    size_t data_size = 0u, data_read = 0u;

    while (read_led(&type, &address_temp, data, &data_size) >= 0)
    {
        switch (type)
        {
        case 0:  // checksum
            if (data_read == 0u)
            {
                data_size = CHUNK_SIZE;
                memset(data, 0xFFu, data_size);
                goto data;
            }
            break;

        case 9:  // address
            address = address_temp;
            data_read = 0u;
            break;

        case 10:  // data
        data:
        {
            size_t n = 0u;
            for (n = 0; data_size - n > 128u; n += 128u)
                emit_srec(3, BASE + address + data_read + n, &data[n], 128u);

            emit_srec(3, BASE + address + data_read + n, &data[n], data_size - n);

            data_read += data_size;

            break;
        }

        case 11:  // block terminator
            if (address != address_temp)
            {
                fprintf(stderr, "block address mismatch (%06" PRIxFAST32 " != %06" PRIxFAST32 ")\n",
                    address, address_temp);
                return 2;
            }

            if (data_read != data_size)
            {
                fprintf(stderr, "data size mismatch (%06" PRIxFAST32 " != %06" PRIxFAST32 ")\n",
                    data_read, data_size);
                return 2;
            }

            data_size = 0u;

            break;

        default:
            fprintf(stderr, "ignoring block type %i at %li\n", type, ftell(stdin));
            break;
        }
    }

    if (!feof(stdin))
    {
        perror("read_led");
        return 1;
    }

    emit_srec(8, 0, NULL, 0);

    return 0;
}
