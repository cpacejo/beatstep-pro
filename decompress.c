#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

int main()
{
    uint8_t out_buf[65536];
    uint8_t* dst = out_buf;

    int hdr;
    while ((hdr = getchar()) != EOF)
    {
        int dat_len = hdr & 7;
        if (dat_len == 0) dat_len = getchar();
        assert(dat_len > 0);
        int run_len = hdr >> 4;
        if (run_len == 0) run_len = getchar();
        const bool copy = (hdr & 8) != 0;

        for (dat_len -= 1; dat_len > 0; --dat_len)
            *(dst++) = getchar();

        if (copy)
        {
            const int copy_dist = getchar();
            assert(copy_dist > 0);
            assert(copy_dist <= dst - out_buf);
            const uint8_t* copy_src = dst - copy_dist;
            for (run_len += 2; run_len > 0; --run_len)
                *(dst++) = *(copy_src++);
        }
        else
        {
            for (; run_len > 0; --run_len)
                *(dst++) = 0;
        }
    }

    fwrite(out_buf, 1, dst - out_buf, stdout);

    return 0;
}
